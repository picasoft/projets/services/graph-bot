#!/bin/sh

if [ -z "${SFTP_USER}" ]; then
  echo "SFTP_USER not set, exiting!"
  exit 1
fi

if [ -z "${SFTP_PASSWORD}" ]; then
  echo "SFTP_PASSWORD not set, exiting!"
  exit 1
fi

echo "Inject SFTP credentials..."
sed "s/SFTP_USER/$SFTP_USER/g; s/SFTP_PASSWORD/$SFTP_PASSWORD/g" /config.json > /config_creds.json

echo "Launching original entrypoint..."
exec /entrypoint.sh
