FROM chosto/graphbot:v1.4.5

COPY ./inject_env.sh /inject_env.sh
CMD [ "/inject_env.sh" ]
